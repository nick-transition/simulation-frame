# Virtual Machine and Framework for Annualized SimulationController

# Pypy
PyPy is a replacement for CPython. It is built using the RPython language that was co-developed with it. The main reason to use it instead of CPython is speed: it runs generally faster (see next section).

# Setup
For this project, all you need is vagrant installed. [Vagrant](https://www.vagrantup.com/)

# Getting Started
```
git clone https://nick-transition@bitbucket.org/nick-transition/simulation-frame.git
cd simulation-frame
```
