from simController import SimulationController

def main():
    # Open an input file
    # Parse out some settings
    simulation = SimulationController("metric",10)
    simulation.runSimulation()

main()
