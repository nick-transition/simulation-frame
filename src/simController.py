
state={}

class SimulationController:
    def __init__(self, units, duration):
        self.units = units
        self.duration = duration

    def annualSimStart(self,state):
        #Place annual sim functions here
        print("Annual simulation starting: "+str(state['year']+1))

    def monthlySim(self,state):
        #Place monthly sim setup functions here
        print("Starting sim for month: "+str(state['month']+1))
        #Get days in month...for now just 30
        for i in range(30):
            state['day']=i
            self.dailySim(state)

        print("Ending monthly sim: " + str(state['month']+1))

    def annualSimEnd(self,state):
        #Place annual sim functions here
        print("Annual simulation ending: "+str(state['year']+1))


    def dailySim(self,state):
        #Place daily sim functions here
        print("Daily sim: "+str(state['day']+1))

    def simulationEngine(self,state):
        if state['year'] > self.duration-1:
            return state

        self.annualSimStart(state)

        for i in range(12):
            state['month']=i
            self.monthlySim(state)

        self.annualSimEnd(state)
        state['year']+=1
        return self.simulationEngine(state)

    def runSimulation(self):
        # Setup the sim
        print(self.duration)

        # initialize simulation state (e.g. backfill data, set up initial conditions of sim)
        state['year'] = 0
        state['month'] = 0
        state['day'] = 0

        # Start the engine!
        finalState = self.simulationEngine(state)
        print(finalState)
